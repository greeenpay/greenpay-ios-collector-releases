![Greenpay](https://greenpay.me/wp-content/uploads/2019/12/LogoColorGreenPay.png)

# Greenpay Device Data Collector #

* Summary: This library is a binary framework that allows iOS apps to collect device and location data.
* Version: 2.0.2
* Deployment target: 9.3+

### How do I get set up? ###

To integrate the package in your app follow this [tutorial](https://developer.apple.com/documentation/xcode/adding_package_dependencies_to_your_app). 

### How do I use it? ###

```swift

import DataCollector

GDataCollector.shared().enableLogs();
GDataCollector.shared().enableSandboxMode();
GDataCollector.shared().collect { (session, success, error) in
    if(success) {
        print(session);
    } else {
        if((error) != nil) {
            print(error!.localizedDescription);
        }
    }
}
```

### Who do I talk to? ###

* [support@greenpay.me](mailto:support@greenpay.me)
