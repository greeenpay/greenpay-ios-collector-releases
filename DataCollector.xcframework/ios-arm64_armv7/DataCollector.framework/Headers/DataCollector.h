//
//  DataCollector.h
//  DataCollector
//
//  Created by Jesús Quirós on 5/23/20.
//  Copyright © 2020 Jesús Quirós. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDataCollector.h"


//! Project version number for DataCollector.
FOUNDATION_EXPORT double DataCollectorVersionNumber;

//! Project version string for DataCollector.
FOUNDATION_EXPORT const unsigned char DataCollectorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DataCollector/PublicHeader.h>
