// swift-tools-version:5.3
import PackageDescription
let package = Package(
    name: "DataCollector",
    platforms: [
        .iOS(.v10)
    ],
    products: [
        .library(
            name: "DataCollector",
            targets: ["DataCollector"])
    ],
    targets: [
        .binaryTarget(
            name: "DataCollector",
            path: "DataCollector.xcframework")
    ])
